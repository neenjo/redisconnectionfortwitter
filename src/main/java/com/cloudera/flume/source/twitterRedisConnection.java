package com.cloudera.flume.source;
import org.apache.flume.EventDrivenSource;
import org.apache.flume.conf.Configurable;
import org.apache.flume.source.AbstractSource;
import org.apache.flume.Context;
import redis.clients.jedis.Jedis;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import org.apache.log4j.Logger;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import java.util.Set;

/**
 * Created by DDR on 7/14/2015.
 */
public class twitterRedisConnection extends AbstractSource implements Configurable,EventDrivenSource {
    private String consumerKey;
    private String consumerSecret;
    private String accessToken;
    private String accessTokenSecret;
    private String host;
    private String port;
    private String authentication;
    Twitter twitter;
    private static final Logger logger =
            Logger.getLogger(twitterRedisConnection.class);


    public void configure(Context context) {
        consumerKey = context.getString(TwitterSourceConstants.CONSUMER_KEY_KEY);
        consumerSecret = context.getString(TwitterSourceConstants.CONSUMER_SECRET_KEY);
        accessToken = context.getString(TwitterSourceConstants.ACCESS_TOKEN_KEY);
        accessTokenSecret = context.getString(TwitterSourceConstants.ACCESS_TOKEN_SECRET_KEY);
        host=context.getString(TwitterSourceConstants.HOST);
        port=context.getString(TwitterSourceConstants.PORT);
        authentication=context.getString(TwitterSourceConstants.AUTHENTICATION);
        logger.info(host+" "+port+" "+authentication);
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setOAuthConsumerKey(consumerKey);
        cb.setOAuthConsumerSecret(consumerSecret);
        cb.setOAuthAccessToken(accessToken);
        cb.setOAuthAccessTokenSecret(accessTokenSecret);
        cb.setJSONStoreEnabled(true);
        cb.setIncludeEntitiesEnabled(true);
        twitter= new TwitterFactory(cb.build()).getInstance();

    }

public void start()
{
    logger.info("starter 001");
    Jedis jedis=new Jedis(host,Integer.parseInt(port));
    jedis.auth(authentication);
    Set<String> memberArray=  jedis.smembers("celebrities");
    User user;
    String string;

    for (String s : memberArray) {

            Set<String> memberArray2=  jedis.smembers(s+"Tw");
        logger.info("for loop 1-"+s);

            for (String s2: memberArray2)
            {
                try {
                    logger.info("Enteres 2nd");
                    user=twitter.showUser(s2);
                    string= String.valueOf(user.getId());
                    logger.info(string);
                    jedis.sadd("twitterid", string);
                    jedis.sadd(string,s);
                } catch (TwitterException e) {
                    logger.info("Twitter id missed for screen name"+s2);
                    jedis.sadd("twitterid","0");
                }

            }



    }
    super.start();
}
}
